#include <iostream>
#include "list.h"

using namespace std;

int main(){

	List List;
	int tmp;
	
	
	List.headPush(8);
	List.headPush(7);
	List.headPush(6);
	List.headPush(24);
	List.headPush(20);
	List.sort();
	List.showAll();
	
	
	cout << "Start : is the list empty? : ";
	
	
	if(theList.isEmpty()){
		cout << "Yes" << endl;	
	}else{
		cout << "No" << endl;	
	}
	
	
	cout << "Add 3, 4, 5 to the head of the list." << endl;
	List.headPush(3);
	List.headPush(4);
	List.headPush(5);
	
	
	cout << "Current list : ";
	theList.showAll();
	
	cout << endl << endl << "Add 2, 1, 0 respectively to the end of the list." << endl;
	List.tailPush(2);
	List.tailPush(1);
	List.tailPush(0);
	cout << "Current list : ";
	
	
	List.showAll();
	cout << endl << endl << "Remove elements from the head and the end of the list." << endl;
	cout << "Removed element : ";
	
	
	tmp = List.headPop();
	cout << tmp << " ";
	
	tmp = List.tailPop();
	
	cout << tmp << endl;
	cout << "Current list : ";
	
	List.showAll();
	cout << endl << endl << "Is there number 3 in the list?: ";
	
	if(List.isInList(3)){
		
		cout << "Yes" << std::endl;
		cout << "Remove 3 from the list." << std::endl;
		List.deleteNode(3);
	}else{
		
		cout << "No" << endl;	
	}
	
	
	cout << "Current list : ";
	List.showAll();
	cout << endl << endl << "-END-";
	
	
	return 0;
}
